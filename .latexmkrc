$pdflatex = 'lualatex -interaction=nonstopmode -synctex=1 %O %S';
$pdf_mode = 1;
$postscript_mode = $dvi_mode = 0;
$pdf_previewer = "start mupdf %O %S";
$pdf_update_method = 2;
$pdf_update_signal = "SIGHUP";
$compiling_cmd = "xdotool search --name %D set_window --name \"%D compiling...\"";
$success_cmd = "xdotool search --name %D set_window --name \"%D OK\";";
$failure_cmd = "xdotool search --name %D set_window --name \"%D FAILURE\";";
