### Install ###
Copy the fonts to your font dir (~/.fonts).
Copy the Scripts to "~/scripts".
Copy the Mira_* folders to "~/.conky".


### Mail ###
The Email part uses the gmail script (Google Mail only).
To set it up edit the gmail.pl script and enter your username and password.


### Weather ###
Type http://xoap.weather.com/search/search?where=[yourcity] to find your
location id in your browser and then add your location id to "<<<YourLocation>>>"
in .conkyrc_weather.


### Sys ###
For the Arch Update part read the Readme in the "arch-updates" folder.
If you don't want it, delete line 85-87 in the .conkyrc_sys.

The Gpu part works with nvidia cards only.
 
 
### Start ###
To start all Conkys at once just make little script called "conky_start" (for example)
in "/usr/bin/" or "/usr/local/bin/".

It should look like this:

#!/bin/bash
conky -c ~/.conky/Mira_green/.conkyrc_clock &
conky -c ~/.conky/Mira_green/.conkyrc_weather &
conky -c ~/.conky/Mira_green/.conkyrc_sys &
conky -c ~/.conky/Mira_green/.conkyrc_net &
conky -c ~/.conky/Mira_green/.conkyrc_disk &
conky -c ~/.conky/Mira_green/.conkyrc_mpd


### Transparency ###
If you use Compiz, you can make Conky transparent.

Open the CompizConfig and go to: General Options > Opacity Settings > New
Enter "class=Conky" and setup the opacity degree.
