${image [PTBI]/expanded-brown-rnd-light.png -p -5,7 -s 490x775}
${voffset -15}${goto 20}${color3}Station: ${color1}[SN]
${goto 20}${color3}Last Update: ${color1}[LU]
${goto 20}${color3}Last Fetch: ${color1}[TD] - [LF] LT
${font DejaVu Sans Mono:style=Bold:size=11}${color1}Present Condition ${color4}${hr 2}${font}
${font DejaVu Sans Mono:style=Bold:size=20}${goto 100}${color1}[CT]${font}
${voffset -25}${goto 3}${font ConkyWeather:size=60}${color1}[PIF]
${voffset -90}${goto 100}${color3}${font DejaVu Sans Mono:style=Bold:size=32}[PT]${font Arrows:size=42}[FCTTF]${font}${font}
${goto 100}${color3}Feels: ${color1}[FL]
${goto 100}${color3}Vis  : ${color1}[VI]${color3}${font Arrows:size=14} [FCTVF]${font}
${goto 100}${color3}UV: ${color1}[UV]${color3}${font Arrows:size=14} [FCTUF]${font}
${voffset -1}${goto 3}${font ConkyWindNESW:size=60}${color1}[BF]${font}
${voffset -65}${goto 100}${color3}Wind : ${color1}[WS] ${color3}${font Arrows:size=14}[FCTWF]${font}
${goto 100}${color3} Gust: ${color1}[WG]
${goto 100}${color3} From: ${color1}[WD]
${voffset 41}${goto 7}${font Moon Phases:size=60}${color1}[MIF]${font}
${voffset -52}${alignc 195}${font DejaVu Sans Mono:style=Bold:size=13}${color5}[MV]${offset 2}${voffset 1}${font Arrows:size=18}[FCTMIF]${font}
${voffset -95}${goto 100}${color3}Moon : ${color1}[MP]${font}
${goto 105}${color3} Age : ${color1}[MA]
${goto 105}${color3} RoC : ${color1}[MROC] %/day
${goto 105}${color3} New : ${color1}[NM]
${goto 105}${color3} FQ  : ${color1}[MFQ]
${goto 105}${color3} Full: ${color1}[FM]
${goto 105}${color3} LQ  : ${color1}[MLQ]
${goto 105}${color3} Rise: ${color1}[MR]${color3}${font Arrows:size=14} [FCTMRF]${font}
${goto 105}${color3} Set : ${color1}[MS]${color3}${font Arrows:size=14} [FCTMSF]${font}
${goto 105}${color3} Tran: ${color1}[MMT]
${voffset -290}${goto 270}${color3}Today: ${color1}[D1C]
${voffset 5}${goto 275}${font ConkyWeather:size=18}${color1}[D1F]${font}${goto 275}
${voffset -30}${goto 310}${color3} Temp: ${color1}[D1T]
${goto 310}${color3} CoR : ${color1}[D1P]
${voffset 0}${goto 270}${color3}Clouds: ${color1}[CLD]
${goto 300}${color3}Rain     : ${color1}[RF]
${goto 300}${color3}Snow     : ${color1}[SD]
${goto 300}${color3}Dew P.   : ${color1}[DP]${color3}${font Arrows:size=14} [FCTDF]${font}
${goto 300}${color3}Humid    : ${color1}[HM]${color3}${font Arrows:size=14} [FCTHF]${font}
${goto 300}${color3}Barometer: ${color1}[BR]${color3}${font Arrows:size=14} [FCTPF]${font}
${voffset 2}${goto 300}${color3}Dur Light: ${color1}[LOV]${color3}${font Arrows:size=14} [FCTLLF]${font}
${goto 300}${color3}Dur Day  : ${color1}[LOD]${color3}${font Arrows:size=14} [FCTLDF]${font}
${goto 300}${color3}Tom DayLn: ${color1}[LODN]${color3}${font Arrows:size=14} [LODNT]${font}
${voffset 2}${goto 300}${color3}Sun Rise : ${color1}[SR]${color3}${font Arrows:size=14} [FCTSRF]${font}
${goto 300}${color3}Sun Set  : ${color1}[SS]${color3}${font Arrows:size=14} [FCTSSF]${font}
${goto 300}${color3}Transit  : ${color1}[SMT]
${goto 300}${color3}CT: ${color1}[CTR]${color3}/${color1}[CTS]
${goto 300}${color3}NT: ${color1}[NTR]${color3}/${color1}[NTS]
${goto 300}${color3}AT: ${color1}[ATR]${color3}/${color1}[ATS]
${font DejaVu Sans Mono:style=Bold:size=11}${color1}Forecast ${color4}${hr 2}${font}
${goto 5}${font ConkyWeather:size=45}${color1}[D1F] ${goto 210}${color1}[D2F]${font}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D1D] ${goto 280}${color3}Date: ${color1}[D2D]
${goto 70}${color3}Temp: ${color1}[D1T]${goto 280}${color3}Temp: ${color1}[D2T]
${goto 70}${color3}Rain: ${color1}[D1P]${goto 280}${color3}Rain: ${color1}[D2P]
${goto 70}${color3}Cond: ${color1}[D1C]${goto 280}${color3}Cond: ${color1}[D2C]${font}

${goto 5}${font ConkyWeather:size=45}${color1}[D3F] ${goto 210}${color1}[D4F]${font}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D3D] ${goto 280}${color3}Date: ${color1}[D4D]
${goto 70}${color3}Temp: ${color1}[D3T]${goto 280}${color3}Temp: ${color1}[D4T]
${goto 70}${color3}Rain: ${color1}[D3P]${goto 280}${color3}Rain: ${color1}[D4P]
${goto 70}${color3}Cond: ${color1}[D3C]${goto 280}${color3}Cond: ${color1}[D4C]${font}

${goto 5}${font ConkyWeather:size=45}${color1}[D5F] ${goto 210}${color1}[D6F]${font}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D5D] ${goto 280}${color3}Date: ${color1}[D6D]
${goto 70}${color3}Temp: ${color1}[D5T]${goto 280}${color3}Temp: ${color1}[D6T]
${goto 70}${color3}Rain: ${color1}[D5P]${goto 280}${color3}Rain: ${color1}[D6P]
${goto 70}${color3}Cond: ${color1}[D5C]${goto 280}${color3}Cond: ${color1}[D6C]${font}

${goto 5}${font ConkyWeather:size=45}${color1}[D7F] ${goto 210}${color1}[D8F]${font}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D7D] ${goto 280}${color3}Date: ${color1}[D8D]
${goto 70}${color3}Temp: ${color1}[D7T]${goto 280}${color3}Temp: ${color1}[D8T]
${goto 70}${color3}Rain: ${color1}[D7P]${goto 280}${color3}Rain: ${color1}[D8P]
${goto 70}${color3}Cond: ${color1}[D7C]${goto 280}${color3}Cond: ${color1}[D8C]${font}
${color4}${hr 2}${font}
