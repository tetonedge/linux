alignment bottom_middle
# alignment top_middle

# default_color white
default_outline_color black
default_shade_color black
double_buffer yes

draw_borders no
draw_graph_borders no
draw_outline no
draw_shades no

no_buffers yes
override_utf8_locale yes


own_window yes
#-----------------------------------------------------------
# change this if you are not getting any conky display
# options are: normal, desktop, dock, panel or override (default: normal)
#-----------------------------------------------------------
own_window_type override
# own_window_type normal
#-----------------------------------------------------------
# uncomment below hints line if not using override
#-----------------------------------------------------------
# own_window_hints below,sticky,undecorated,skip_taskbar,skip_pager
# own_window_hints below,sticky,skip_taskbar,skip_pager

# own_window_colour grey
own_window_transparent yes
# own_window_class Conky

# ARGB can be used for real transparency,
# note that a composite manager is required for real transparency
# own_window_argb_visual yes

# Set the transparency alpha value from 0 to 255
# own_window_argb_value 100

# color defs - change as required
color1 Tan1
color2 Gray
color3 White
color4 DarkSlateGray
color5 yellow
color6 black
color7 darkgrey
color8 cyan

# maximum_width 470
# minimum_size 500 1
maximum_width 900
# minimum_size 900 200
minimum_size 900 1040

use_xft yes
xftalpha 0.8
#-----------------------------------------------------------
# Change the xftfont size value if the template is not
# appearing aligned
#-----------------------------------------------------------
# xftfont DejaVu Sans:size=12
xftfont DejaVu Sans:size=12:style=bold
# xftfont DroidSansFallback:size=10
# xftfont DejaVu Sans Mono:size=12
# xftfont DroidSans:size=12
# xftfont FreeSerif:size=15
# xftfont FreeSans:size=10

total_run_times 0

# this value is in bytes - do not sting over this ;-)
# text_buffer_size 1000
# text_buffer_size 50000
update_interval 3

short_units yes

gap_x -150
gap_y 0

imlib_cache_size 0


#-----------------------------------------------------------
# location of image file - conkywx tmp directory for the used conkywx config file
# IMPORTANT do not comment this line - even if you do not use it
#-----------------------------------------------------------
template1 /tmp/wx-param-conkywx-02.conf-pChA_r_27u0-m39/gsi_image.gif
template2 /tmp/wx-param-conkywx-02.conf-pChA_r_27u0-m39/gwsi_image.gif


TEXT
${if_existing ${template1}} ${image ${template1} -p 0, 0 -s 300x200}${else}No CIMSS Surface Analysis Image${endif}
${if_existing ${template2}} ${image ${template2} -p 0, 210 -s 300x200}${else}${goto 310}No Wund Surface Analysis Image${endif}
# ${if_existing ${template1}} ${image ${template1} -p 0, 0 -s 700x500}${else}No CIMSS Surface Analysis Image${endif}
# ${if_existing ${template2}} ${image ${template2} -p 0, 510 -s 700x500}${else}${goto 310}No Wund Surface Analysis Image${endif}


