alignment tl

default_color white
default_outline_color black
default_shade_color black
double_buffer yes

draw_borders no
draw_graph_borders no
draw_outline no
draw_shades yes

no_buffers yes
override_utf8_locale yes

own_window yes
#-----------------------------------------------------------
# change this if you are not getting any conky display
# options are: normal, desktop, dock, panel or override (default: normal)
#-----------------------------------------------------------
own_window_type desktop
#-----------------------------------------------------------
# uncomment below hints line if not using override
#-----------------------------------------------------------
own_window_hints below,sticky,undecorated,skip_taskbar,skip_pager

# own_window_colour black
own_window_transparent yes

# ARGB can be used for real transparency,
# note that a composite manager is required for real transparency
# own_window_argb_visual true

# Set the transparency alpha value from 0 to 255
# own_window_argb_value 160

# color defs - change as required
color1 Tan1
color2 Gray
color3 White
color4 DarkSlateGray
color5 yellow
color6 black
color7 darkgrey
color8 cyan

maximum_width 470
minimum_size 500 1

use_xft yes
xftalpha 0.8
#-----------------------------------------------------------
# Change the xftfont size value if the template is not
# appearing aligned
#-----------------------------------------------------------
xftfont DejaVu Sans Mono:size=10

total_run_times 0

text_buffer_size 7000
update_interval 3

short_units yes
# pad_percents 2

# Fonts used
# ConkyWindNESW, ConkyWeather, Moon Phases, DejaVu Sans Mono

#-----------------------------------------------------------
# gap x and y - y component uncomment with required template
#-----------------------------------------------------------
gap_x 69

lua_load /usr/share/conkywx/lib/conkywx.lua

#-----------------------------------------------------------
# template0 points to the main program conkywx
#-----------------------------------------------------------
template0 /usr/share/conkywx/conkywx.sh

#-----------------------------------------------------------
# template1 points to the conkywx config file
# single config file for both image and font templates
#-----------------------------------------------------------
template1 /home/jdtuck/.conkywx/conkywx.conf

#-----------------------------------------------------------
# template2 points to the template to use
# comment the template you do not want to use
#-----------------------------------------------------------
#### font template section ---------------------------------
# template2 ./examples/expanded-font-template.sh
# gap_y -435
#### image template section --------------------------------
template2 /home/jdtuck/.conkywx/expanded-image-template.sh
#gap_y -390
imlib_cache_size 0

#-----------------------------------------------------------
# location of wxalert file - conkywx tmp directory for the used conkywx config file
# IMPORTANT do not comment this line - even if you do not use it
#-----------------------------------------------------------
template3 /tmp/wx-param-conkywx-02.conf-pChA_r_27u0-m39/wxalert

TEXT
${voffset -14}${lua main -n background -r 25 -c 0x000000:0.2 -p NA,NA}
${font :style=Bold:size=11}${color1}Weather Real${if_existing ${template3}} \
${color8}Alert:${lua main -n scroller -f 14 -l 36 -s 5 -p 170,20 -o ${template3}}${else}${color4}${hr 2}${color8}${endif}${font}
${execpi 900 "${template0}" -c "${template1}" -t "${template2}"}

