# ${image [PTBI]/short-brown-rnd-light.png -p -5,0 -s 487x800}
${voffset 0}${goto 20}${color3}Location: ${color1}[LN]
${goto 20}${color3}Last Update: ${color1}[LU]
${goto 20}${color3}Last Fetch: ${color1}[TD] - [LF] LT
${font DejaVu Sans Mono:style=Bold:size=11}${color1}Present Condition ${color4}${hr 2}${font}
${voffset 0}${font DejaVu Sans Mono:style=Bold:size=20}${goto 105}${color1}[CT]${font}
# ${voffset 100}${goto 10}
${voffset 0}${goto 105}${color3}${font DejaVu Sans Mono:style=Bold:size=32}[PT]${font Arrows:size=42}[FCTTF]${font}${font}
${goto 105}${color3}Feels: ${color1}[FL]
${goto 105}${color3}Vis  : ${color1}[VI]${color3}${font Arrows:size=14} [FCTVF]${font}
${goto 105}${color3}UV: ${color1}[UV]${color3}${font Arrows:size=14} [FCTUF]${font}
${if_existing [BI]}${voffset 45}${image [BI] -p 0, 200 -s 95x95}${else}${voffset -9}${goto 10}${font ConkyWindNESW:size=60}${color1}[BF]${font}${endif}
${voffset -55}${goto 105}${color3}Wind${font} : ${color1}[WS] ${color3}${font Arrows:size=14}[FCTWF]${font}
${goto 105}${color3} Gust: ${color1}[WG]
${goto 105}${color3} From: ${color1}[WD]
# ${voffset 105}${goto 10}
${voffset 70}${font DejaVu Sans Mono:style=Bold:size=16}${alignc 186}${color5}[MV]${font}${offset 2}${voffset -6}${font Arrows:size=21}[FCTMIF]${font}
${voffset -95}${goto 105}${color3}Moon : ${color1}[MP3]
${goto 105}${color3} Age : ${color1}[MA]
${goto 105}${color3} RoC : ${color1}[MROC] %/day
${goto 105}${color3} New : ${color1}[NM]
${goto 105}${color3} FQ  : ${color1}[MFQ]
${goto 105}${color3} Full: ${color1}[FM]
${goto 105}${color3} LQ  : ${color1}[MLQ]
${goto 105}${color3} Rise: ${color1}[MR]${color3}${font Arrows:size=14} [FCTMRF]${font}
${goto 105}${color3} Set : ${color1}[MS]${color3}${font Arrows:size=14} [FCTMSF]${font}
${goto 105}${color3} Tran: ${color1}[MMT]
# ${exec echo "[METAR]" | exec fold -s -w 55}
# start of 2nd column
${voffset -298}${goto 265}${goto 271}${color3}[D1D]: ${color1}[D1C]
${goto 306}${color3} Temp: ${color1}[D1T]
${goto 306}${color3} CoR : ${color1}[D1P]
${voffset 3}${goto 270}${color3}Clouds: ${color1}[CLD]
${goto 300}${color3}Rain     : ${color1}[RF]
${goto 300}${color3}Snow     : ${color1}[SD]
${goto 300}${color3}DP       : ${color1}[DP]${color3}${font Arrows:size=14} [FCTDF]${font}
${goto 300}${color3}Humid    : ${color1}[HM]${color3}${font Arrows:size=14} [FCTHF]${font}
${voffset 3}${goto 300}${color3}Bar      : ${color1}[BR]${color3}${font Arrows:size=14} [FCTPF]${font}
${voffset 3}${goto 300}${color3}Dur Light: ${color1}[LOV]${color3}${font Arrows:size=14} [FCTLLF]${font}
${goto 300}${color3}Dur Day  : ${color1}[LOD]${color3}${font Arrows:size=14} [FCTLDF]${font}
${goto 300}${color3}Tom DayLn: ${color1}[LODN]${color3}${font Arrows:size=14} [LODNT]${font}
${voffset 3}${goto 300}${color3}Sun Rise : ${color1}[SR]${color3}${font Arrows:size=14} [FCTSRF]${font}
${goto 300}${color3}Sun Set  : ${color1}[SS]${color3}${font Arrows:size=14} [FCTSSF]${font}
${goto 300}${color3}Transit  : ${color1}[SMT]
${goto 300}${color3}CT: ${color1}[CTR]${color3}/${color1}[CTS]
${goto 300}${color3}NT: ${color1}[NTR]${color3}/${color1}[NTS]
${goto 300}${color3}AT: ${color1}[ATR]${color3}/${color1}[ATS]
${font DejaVu Sans Mono:style=Bold:size=11}${color1}Forecast ${color4}${hr 2}${font}
${voffset 50}${goto 5}${image [D1I] -p 0,440 -s 60x60} ${goto 5}${image [D2I] -p 210,440 -s 60x60}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D2D] ${goto 280}${color3}Date: ${color1}[D3D]
${goto 70}${color3}Temp: ${color1}[D2T]${goto 280}${color3}Temp: ${color1}[D3T]
${goto 70}${color3}Rain: ${color1}[D2P]${goto 280}${color3}Rain: ${color1}[D3P]
${goto 70}${color3}Cond: ${color1}[D2C]${goto 280}${color3}Cond: ${color1}[D3C]${font}

${voffset 40}${goto 5}${image [D3I] -p 0,520 -s 60x60} ${goto 5}${image [D4I] -p 210,520 -s 60x60}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D4D] ${goto 280}${color3}Date: ${color1}[D5D]
${goto 70}${color3}Temp: ${color1}[D4T]${goto 280}${color3}Temp: ${color1}[D5T]
${goto 70}${color3}Rain: ${color1}[D4P]${goto 280}${color3}Rain: ${color1}[D5P]
${goto 70}${color3}Cond: ${color1}[D4C]${goto 280}${color3}Cond: ${color1}[D5C]${font}

${voffset 40}${goto 5}${image [D5I] -p 0,600 -s 60x60} ${goto 5}${image [D6I] -p 210,600 -s 60x60}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D6D] ${goto 280}${color3}Date: ${color1}[D7D]
${goto 70}${color3}Temp: ${color1}[D6T]${goto 280}${color3}Temp: ${color1}[D7T]
${goto 70}${color3}Rain: ${color1}[D6P]${goto 280}${color3}Rain: ${color1}[D7P]
${goto 70}${color3}Cond: ${color1}[D6C]${goto 280}${color3}Cond: ${color1}[D7C]${font}

${voffset 40}${goto 5}${image [D7I] -p 0,680 -s 60x60} ${goto 5}${image [D8I] -p 210,680 -s 60x60}

${voffset -65}${goto 70}${font DejaVu Sans Mono:size=10}${color3}Date: ${color1}[D8D] ${goto 280}${color3}Date: ${color1}[D9D]
${goto 70}${color3}Temp: ${color1}[D8T]${goto 280}${color3}Temp: ${color1}[D9T]
${goto 70}${color3}Rain: ${color1}[D8P]${goto 280}${color3}Rain: ${color1}[D9P]
${goto 70}${color3}Cond: ${color1}[D8C]${goto 280}${color3}Cond: ${color1}[D9C]${font}
${color4}${hr 2}${font}
# All images .
# The present condition image
${image [PIC] -p 0,		 95   	-s   95x95}
# The moon image
${image [MIC] -p 9,		310   	-s   85x85}
${lua main -n wxgraph -p 193,170 -d [GT] -c 0xEE4000:1}#
${lua main -n wxgraph -p 320,220 -d [GD] -c 0x00FFFF:1}#
${lua main -n wxgraph -p 320,257 -d [GP] -c 0x00FF00:1}#
# Today image
# ${image [D1I] -p 268,	126   	-s   30x30}

