#!/bin/bash
# -----------------------------------------------------------------------
# Export path statements for use in *conky* config file
# if you move this file else where then give the full path for WX_DIR variable
# -----------------------------------------------------------------------
# WX_DIR="/usr/share/conkywx"; export WX_DIR

# -----------------------------------------------------------------------
# stop existing conky processes
# -----------------------------------------------------------------------
# if [[ "$(pidof conky)" ]] ; then killall conky; fi

# -----------------------------------------------------------------------
# run conky instances
# -----------------------------------------------------------------------
conky &

# conky -c ./examples/vindsl-conkyrc &

conky -c ~/.conkywx/expanded-conkyrc.sh &

#conky -c ./examples/short-conkyrc.sh &

# sleep 1
#conky -c ./examples/short-02-conkyrc.sh &
# conky -c ./examples/short-luamix-conkyrc.sh &
# conky -c ./examples/big-luamix-conkyrc.sh &

#conky -c ./examples/conkywx-now-conkyrc.sh &

# conky -c ./examples/scroller-conkyrc.sh &

#conky -c ./examples/slim-conkyrc.sh &


exit
