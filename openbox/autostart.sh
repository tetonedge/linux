tint2 &
#cairo-compmgr &
xcompmgr -cCfF -t-5 -l-5 -r4.2 -o.55 -D3 &
nitrogen --restore & 
wicd-client 
dbus-launch gnome-power-manager &
parcellite -n &
tracker-status-icon &
conky
SpiderOak &
xscreensaver &
volti &
mpd ~/.mpd/mpd.conf
blueman-applet &
export OOO_FORCE_DESKTOP=gnome
