#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk, sys, re, os, config
from coe import *

# Display Message Box (e.g. Help Screen, Warning Screen, etc.)
class MessageBox(gtk.MessageDialog):
        def __init__(self, parent, text, type=gtk.MESSAGE_INFO):
                gtk.MessageDialog.__init__(self, parent, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT, type, gtk.BUTTONS_OK)
		self.connect("key-release-event", self.event_key)
                self.set_markup(text)
                self.set_default_response(gtk.RESPONSE_OK)
                self.connect("response", self.clicked)
		self.box = gtk.Frame()
                self.box.set_border_width(0)
                self.box.set_shadow_type(gtk.SHADOW_IN)
		self.vbox.reparent(self.box)
		self.add(self.box)
		self.box.show_all()
        def clicked(self, args, id):
                self.destroy()
	def event_key(self, args, event):
		if event.keyval == gtk.keysyms.F12 or event.keyval == gtk.keysyms.Escape:
			self.destroy()


# Display Dialog Box
class TwoFactorDialog(gtk.Dialog):
        def __init__(self, parent, data=[]):
                gtk.Dialog.__init__(self, "Two Factor Authentication", parent, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT, (gtk.STOCK_HELP, gtk.RESPONSE_HELP, gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))
                self.set_position(gtk.WIN_POS_CENTER)
                self.connect("key-release-event", self.event_key)
                self.set_default_response(gtk.RESPONSE_OK)
                self.connect("response", self.clicked)
                self.label = gtk.Label("<span font_family='Airal' weight='bold'>Two Factor Authentication</span>")
                self.label.set_use_markup(True)
                self.vbox.add(self.label)
                self.label.show()
                self.selection = 'N'
                self.uname = gtk.HBox()
                self.label = gtk.Label("         Username: ")
                self.uname.pack_start(self.label, False, True, 0)
                self.username = gtk.Entry(100)
                self.username.set_size_request(225,-1)
                self.uname.pack_start(self.username, False, True, 0)
                self.vbox.add(self.uname)
                self.entity = gtk.HBox()
                self.label = gtk.Label("   Entity Account: ")
                self.entity.pack_start(self.label, False, True, 0)
                self.entity_account = gtk.Entry(100)
                self.entity_account.set_size_request(225,-1)
                self.entity.pack_start(self.entity_account, False, True, 0)
                self.vbox.add(self.entity)
                if len(data) > 0:
                        if data[0] == 'N':
                                self.selection = 'N'
                        else:
                                self.selection = 'Y'
                self.box = gtk.Frame()
                self.box.set_border_width(0)
                self.box.set_shadow_type(gtk.SHADOW_IN)
                self.vbox.reparent(self.box)
                self.add(self.box)
                # Sets Defaults
                self.username.set_flags(gtk.CAN_FOCUS)
                self.username.set_flags(gtk.HAS_FOCUS)
                self.username.set_flags(gtk.CAN_DEFAULT)
                self.username.grab_focus()
                if len(data) > 0:
                        self.username.set_text(data[1])
                        self.userid.set_text(data[2])
                        self.entity_account.set_text(data[3])
                self.box.show_all()
        def clicked(self, args, id):
                # Clicked Okay
                if id == gtk.RESPONSE_OK:
                        error_count = 0
                        error_text = "<b>Input Error</b>\n\nThe following fields have errors:\n\n"
                        self.verify = Verification()
                        if self.verify.check_username(self.username.get_text()) == False:
                                error_text += " - User Name\n"
                                error_count += 1
                        if self.verify.check_username(self.entity_account.get_text()) == False:
                                error_text += " - Entity Account\n"
                                error_count += 1
                        if error_count == 0:
                                self.selection = 'Y'
                                self.hide_all()
                        else:
                                dialog = MessageBox(self, error_text, gtk.MESSAGE_ERROR)
                                dialog.show_all()
                elif id == gtk.RESPONSE_CANCEL:
                        self.selection = 'N'
                        coe.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] = 'N'
                        coe.refresh_menus()
                        self.hide_all()
                # Clicked on Help
                else:
                        self.show_help()
        def show_help(self):
 		help_text = ("""
<b>Two Factor Authenication</b>\n
Two factor authentication requires the following input to work\n
	- Userame
	- Entity Account\n
If you select to configure Two Factor Authentication later the COE will lay out the scripts with no configuration. You may configure Two Factor Authentication later through the command line or graphical tools.""")               
		dialog = MessageBox(self, help_text, gtk.MESSAGE_INFO)
                dialog.show_all()
	def event_key(self, args, event):
		if event.keyval == gtk.keysyms.F12 or event.keyval == gtk.keysyms.Escape:
			self.hide_all()
		elif event.keyval == gtk.keysyms.F1:
			self.show_help(text)


# Display Main Menu
class MainMenu:
        def __init__(self):
		# LAN LIST Variable
		self.avaliable_lans = ",srn,son,scn,srn_ca,son_ca,scn_ca,nolan"

                # COE Library Functions
                self.cfg = Configuration()
                self.verify = Verification()

                # Application Data
                self.data = {}
                self.data = self.cfg.get_env_group("DEFAULT_COE")
		self.data_original = {}
		self.data_original = self.cfg.get_env_group("DEFAULT_COE")

                # Create Main Window
                self.window = gtk.Window()
                self.window.set_title("Sandia National Labs - Red Hat Enterprise Linux COE")
                self.window.set_position(gtk.WIN_POS_CENTER)
		self.window.set_icon_from_file(self.get_base_path()+'/sandia16.png')
		self.window.connect("delete_event",self.quit)
		self.window.connect("key-release-event",self.event_key)
		self.display = gtk.gdk.display_get_default()
		self.screen = self.display.get_default_screen()
		self.hres = self.screen.get_width()
		self.vres = self.screen.get_height()

                # Create Main Vertical Box to Populate
                self.vbox = gtk.VBox()

	        # Creates Logo and COE Header
                self.header = gtk.HBox()

                self.logo = gtk.Image()
                self.logo.set_from_file(self.get_base_path()+'/snl_logo.png')
                self.header.add(self.logo)

		self.header_text = gtk.VBox()
		hostname = os.popen('hostname')
                self.label = gtk.Label("<span font_family='Airal' weight='bold' foreground='red' size='large'>   Red Hat Enterprise Linux COE   </span>\n<b>System Configuration</b>")
                self.label.set_use_markup(True)
		self.label.set_justify(gtk.JUSTIFY_CENTER)
                self.header.add(self.label)
 		self.vbox.add(self.header)

		# System Label
		self.label = gtk.Label("<i>%s</i>"%(hostname.readline()))
		self.label.set_use_markup(True)
		self.vbox.pack_start(self.label, False, False, 0)

		# Display COE Options
		for item in config.Option.keys():
                        exec("self.Option%d = gtk.CheckButton(config.Option[%d][0])"%(item,item))
			if eval("len(config.Option[%d])"%(item)) > 2:
				if eval("config.Option[%d][2]"%(item)) != None:
					self.hbox = gtk.HBox()
					exec("parent = config.Option[%d][2]"%(item))
					while 1:
						if parent == None:
							break
						else:
							exec("parent = config.Option[%d][2]"%(parent))
						self.label = gtk.Label("\t")
						self.hbox.pack_start(self.label,False,False,0)
					exec("self.hbox.add(self.Option%d)"%(item))
					self.vbox.pack_start(self.hbox,False,False,0)
				else:
					exec("self.vbox.pack_start(self.Option%d,False,True,0)"%(item))
			else:
				exec("self.vbox.pack_start(self.Option%d,False,True,0)"%(item))
                        exec("self.Option%d.show()"%(item))
			if self.data[config.Option[item][1]] == 'Y':
                                exec("self.Option%d.set_active(True)"%(item))
                        elif self.data[config.Option[item][1]] == 'N':
                                exec("self.Option%d.set_active(False)"%(item))
		self.check_state('Option')
		self.update_config()
		for item in config.Option.keys():
			exec("self.Option%d.connect('toggled', self.update_exceptions, 'Option', %d)"%(item,item))

		# Blank Label
		label = gtk.Label("")
		self.vbox.pack_start(label, False, False, 0)

		# Network Selection
                self.lan = gtk.HBox()
                self.lan_label = gtk.Label("   Please Select Network: ")
                self.lan.pack_start(self.lan_label, False, True, 0)
                self.lan_list = self.avaliable_lans.split(',')
                self.lan_select = gtk.combo_box_new_text()
		for index, net in enumerate(self.lan_list):
                        self.lan_select.append_text(net)
			if self.data["DEFAULT_COE_LAN"].lower() == net:
				self.lan_select.set_active(index)
                self.lan.pack_start(self.lan_select, False, True, 0)
                self.vbox.add(self.lan)

                # Button Bar at the Bottom of the Window
                self.button_bar = gtk.HBox()

                # Apply COE
                self.button1 = gtk.Button(None, gtk.STOCK_APPLY)
                self.button1.connect("clicked", self.apply_coe)
                self.button_bar.pack_end(self.button1, False, True, 0)

                # Help
                self.button2 = gtk.Button(None, gtk.STOCK_HELP)
                self.button2.connect("clicked", self.show_help)
                self.button_bar.pack_end(self.button2, False, True, 0)

                # Quit - Debug - Progam Quits in Apply COE
                self.button3 = gtk.Button(None, gtk.STOCK_QUIT)
                self.button3.connect("clicked", self.quit)
                self.button_bar.pack_end(self.button3, False, True, 0)

                self.vbox.add(self.button_bar)

                self.window.add(self.vbox)
                self.window.show_all()


	# Key Press Event
	def event_key(self, args, event):
		if event.keyval == gtk.keysyms.F12:	
			self.apply_coe(args)
		elif event.keyval == gtk.keysyms.F1:
			self.show_help_main(args)

	# Get Base Path
	def get_base_path(self):
		index = sys.argv[0].rfind('/')
 		path = sys.argv[0][0:index]
		return path

        # Shows Two Factor Authentication Configuration
        def show_two_factor(self):
		try:
			if self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] == 'Y':
				self.two_factor = TwoFactorDialog(self.window,['Y',self.data['DEFAULT_COE_TWO_FACTOR_USERNAME'],self.data['DEFAULT_COE_TWO_FACTOR_ENTITY']])
			elif self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] == 'N':
				self.two_factor = TwoFactorDialog(self.window,['N','',''])
		except:
			self.two_factor = TwoFactorDialog(self.window)
		self.two_factor.show_all()
		

	# Updates Exceptions by checking current State
	def check_state(self, var):
		for i in eval("config.%s.keys()"%(var)):
			if eval("len(config.%s[%d])"%(var,i)) > 3:
				if eval("config.%s[%d][3]"%(var,i)) != None:
					for j in eval("config.%s[%d][3]"%(var,i)):
						for k in eval("config.Exception[%d][2]"%(j)):
							if eval("self.%s%d.get_active()"%(var,i)) == True:
								if eval("config.Exception[%d][1]"%(j)) == "default_on":
									exec(eval("'self.%s%d.set_active(True)'"%(var,k)))
									exec(eval("'self.%s%d.set_sensitive(False)'"%(var,k)))
							elif eval("self.%s%d.get_active()"%(var,i)) == False:
								if eval("config.Exception[%d][1]"%(j)) == "default_off":
									exec(eval("'self.%s%d.set_active(False)'"%(var,k)))
									exec(eval("'self.%s%d.set_sensitive(False)'"%(var,k)))
								elif eval("config.Exception[%d][1]"%(j)) == "off":
									exec(eval("'self.%s%d.set_active(False)'"%(var,k)))
									exec(eval("'self.%s%d.set_sensitive(False)'"%(var,k)))


	# Runs Through Exceptions Defined in Configuration File	
	def update_exceptions(self, args, var, item):
		if eval("len(config.%s[%d])"%(var,item)) > 3:
			if eval("config.%s[%d][3]"%(var,item)) != None:
				for i in eval("config.%s[%d][3]"%(var,item)):
					for j in eval("config.Exception[%d][2]"%(i)):
						if eval("self.%s%d.get_active()"%(var,item)) == True:
							if eval("config.Exception[%d][1]"%(i)) == "checked" and eval("self.%s%d.state"%(var,j)) != gtk.STATE_INSENSITIVE:
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "unchecked" and eval("self.%s%d.state"%(var,j)) != gtk.STATE_INSENSITIVE:
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(False)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "disabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(False)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "enabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
								if eval(eval("'self.data_original[config.'+config.Exception[%d][0]+'[%d][1]]'"%(i,j))) == 'Y':
									exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(True)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(False)'"%(i,j)))
						elif eval("self.%s%d.get_active()"%(var,item)) == False:
							if eval("config.Exception[%d][1]"%(i)) == "disabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(False)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(False)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(False)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(False)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(False)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.set_sensitive(True)'"%(i,j)))
								if eval(eval("'self.data_original[config.'+config.Exception[%d][0]+'[%d][1]]'"%(i,j))) == 'Y':
									exec(eval("'self.'+config.Exception[%d][0]+'%d.set_active(True)'"%(i,j)))	
						# Deals with childern
						if eval(eval("'len(config.'+config.Exception[%d][0]+'[%d])'"%(i,j))) > 3:
							if eval(eval("'config.'+config.Exception[%d][0]+'[%d][3]'"%(i,j))) != None:
								for k in eval(eval("'config.'+config.Exception[%d][0]+'[%d][3]'"%(i,j))):
									exec("self.update_exceptions(None, '%s',%d)"%(var,j))

		# Check State of Congfiguration
		exec("self.check_state('%s')"%(var))

		# Update Configuration
		self.update_config()

        # Updates Configuration for Menu Items
        def update_config(self):
		# Update COE Option Menu
                for item in config.Option.keys():
			if eval("self.Option%d.get_active()"%(item)) == True:
				if eval("len(config.Option[%s])"%(item)) > 4 and eval("self.data[config.Option[%s][1]]"%(item)) == 'N':
					exec(eval("config.Option[%s][4]"%(item)))
				exec("self.data.update({config.Option[%s][1]:'Y'})"%(item))
                        else:
				exec("self.data.update({config.Option[%s][1]:'N'})"%(item))

	# Refresh Menu Option
        def refresh_menus(self):
		for item in config.Option.keys():
			if self.data[config.Option[item][1]] == 'Y':
                                exec("self.Option%d.set_active(True)"%(item))
                        elif self.data[config.Option[item][1]] == 'N':
                                exec("self.Option%d.set_active(False)"%(item))
		self.update_config()

        # Shows Help for Main Install
        def show_help(self, args):
                help_text = ("<b>Linux COE Configuration</b>\n\n"+
                            "Applies Sandia approved configuration to an existing Red Hat Enterprise Linux Installation.\n"+
                            "\nRefer to the COE Documentation for further explantion:\n\n"+
                            " http://redhatcoe.sandia.gov")
                dialog = MessageBox(self.window, help_text, gtk.MESSAGE_INFO)
                dialog.show_all()

        # Appply COE to Kickstart File
        def apply_coe(self, args):
		# Make sure a LAN was selected
		if self.lan_select.get_active_text() == "":
			dialog = MessageBox(self.window, "<b>Please Select a LAN!</b>", gtk.MESSAGE_ERROR)
			dialog.show_all()
			return False

		# Apply COE LAN
		self.data['DEFAULT_COE_LAN'] = self.lan_select.get_active_text()

		# Two-Factor Authentication
		if self.data['DEFAULT_COE_KERBEROS_TWO_FACTOR'] == 'Y':
			try:
				if self.two_factor.selection == 'Y':
					self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] = 'Y'
					self.data['DEFAULT_COE_TWO_FACTOR_USERNAME'] = self.two_factor.username.get_text()
					self.data['DEFAULT_COE_TWO_FACTOR_ENTITY'] = self.two_factor.entity_account.get_text()
				else:
					self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] = 'N'
			except:
				pass
				

		self.cfg.set_config("/etc/sandia/.ksswitches", self.data)
		gtk.main_quit
		sys.exit(0)

	# Exits the Linux COE Menu
	def quit(self,args,data=None):
		gtk.main_quit
		sys.exit(1)

# Executes Window Display
if __name__ == "__main__":
        coe = MainMenu()
        gtk.main()
