#!/usr/bin/python

PROGNAME='media-prompt-text'

from snack import *
import gettext
import sys
gettext.bindtextdomain(PROGNAME, "/usr/share/locale")
gettext.textdomain(PROGNAME)

try:
    gettext.install(PROGNAME, "/usr/share/locale", 1)
    import __builtin__
    __builtin__.__dict__['_'] = gettext.gettext
except IOError:
    import __builtin__
    __builtin__.__dict__['_'] = unicode

def runItStand(screen):

        prompt_msg = (_("You have reached this screen due to one of the following reasons\n\n" +
                        " - You are installing the Linux COE from Standalone Media\n" +
                        " - The Standalone Media is OUT OF DATE\n" +
                        " - This workstation is not configured for the SRN LAN\n\n" +
                        "Note:   The Standalone Media is required for the SON/SCN\n\n" +
                        "The network or COE server could not be reached (Perhaps the network cable is " +
                        "unplugged, the network is not configured properly, the " +
                        "network is down or firewalled).\n\n"+
                        "You may either insert the StandAlone CD, " +
                        "check network cables or configuration and press OK to try again, or Quit now " +
                        "(you can always run the 'linuxcoe' script again). "))

        prompt = TextboxReflowed(59,_(prompt_msg),flexDown=0,flexUp=0)
        bb2=ButtonBar(screen,((_("Ok"),"continue","F12"),((_("Quit"),"quit"))))
        tl2=GridForm(screen,_("Please insert StandAlone CD"),1,3)
        tl2.add(prompt,0,0,(0,0,0,1))
        tl2.add(bb2,0,1,growx=1)
        while 1:
           res2 = tl2.run()
           if bb2.buttonPressed(res2)=="continue":
                return 0
           if bb2.buttonPressed(res2)=="quit":
                screen.finish()
                sys.exit(1)

screen = SnackScreen()
runItStand(screen)
screen.finish()
