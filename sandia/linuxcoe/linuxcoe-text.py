#! /usr/bin/python

import os, sys, config
from coe import *
from snack import *

class MainMenu:
        def __init__(self):
		# LAN LIST Variable
		self.avaliable_lans = ",srn,son,scn,srn_ca,son_ca,scn_ca,nolan"

		# COE Library Functions
                self.cfg = Configuration()
                self.verify = Verification()

                # Application Data
                self.data = {}
                self.data = self.cfg.get_env_group("DEFAULT_COE")
		self.data_original = {}
		self.data_original = self.cfg.get_env_group("DEFAULT_COE")

		# Initialize Option Menu
                for item in reversed(config.Option.keys()):
                        exec("self.Option%d = Checkbox('')"%(item))
			exec("self.Option%d_disabled = 0"%(item))
			if self.data[config.Option[item][1]] == 'Y':
                                exec("self.Option%d.setValue('*')"%(item))
                        elif self.data[config.Option[item][1]] == 'N':
                                exec("self.Option%d.setValue(' ')"%(item))

		# Main Screen
		self.screen = SnackScreen()
		self.screen.drawRootText(1,1,"Linux COE Configuration")
		self.screen.pushHelpLine("  F1-Help | <Tab> between items | <Space> select | F12-Continue")

		# Screen Grids for Snack Layout
		self.input_length = len(config.Option)+1
		g1=Grid(2,self.input_length)

		for item in config.Option.keys():
			self.label = Label(" ")
			g1.setField(self.label,0,item,(0,0,0,0),anchorLeft=1)

			subgrid = Grid(2,1)
			exec("self.Option%d = Checkbox(config.Option[%d][0])"%(item,item))
			exec("self.Option%d_disabled = 0"%(item))
			if self.data[config.Option[item][1]] == 'Y':
                                exec("self.Option%d.setValue('*')"%(item))
                        elif self.data[config.Option[item][1]] == 'N':
                                exec("self.Option%d.setValue(' ')"%(item))
			self.label = Label("")
			spacing = ""
			if eval("len(config.Option[%d])"%(item)) > 2:
				if eval("config.Option[%d][2]"%(item)) != None:
					exec("parent = config.Option[%d][2]"%(item))
					while 1:
						if parent == None:
							break
						else:
							exec("parent = config.Option[%d][2]"%(parent))
						spacing += "   "
			self.label.setText(spacing)
			subgrid.setField(self.label,0,0,(0,0,0,0))
			exec("subgrid.setField(self.Option%d,1,0,(0,0,0,0),anchorLeft=1)"%(item))
			g1.setField(subgrid,1,item,(0,0,0,0),anchorLeft=1)
		self.check_state('Option')
		for item in config.Option.keys():
			exec("self.Option%d.setCallback(self.update_exceptions,'Option:%d')"%(item,item))

		# Network Selection
		self.lan_select = CListbox(1,20,[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],scroll=1,col_pad=0)
		for lan in self.avaliable_lans.split(','):
		   	self.lan_select.append(lan,lan)
			if self.data['DEFAULT_COE_LAN'].lower() == lan:
				self.lan_select.setCurrent(lan)
		subgrid = Grid(2,1)
		subgrid.setField(Label ("Please Select Network: "),0,0)
		subgrid.setField(self.lan_select,1,0,(0,1,0,1),anchorRight=1)
		self.label = Label(" ")
		g1.setField(self.label,0,self.input_length-1,(0,0,0,0),anchorLeft=1)
		g1.setField(subgrid,1,self.input_length-1,(0,0,0,0),anchorLeft=1)

		tl = GridForm(self.screen,"Linux COE Configuration - Main Menu",1,2)
		bb = ButtonBar(self.screen,((("Apply COE"),"ok","F12"),(("Quit"),"quit","F11"),(("Help"),"help","F1")))

		tl.add(g1,0,0,(0,0,0,0),anchorLeft=1)
		tl.add(bb,0,1,growx=0)
		
		while 1:
			res = tl.run()
			if bb.buttonPressed(res)=="ok":
				self.apply_coe()
			if bb.buttonPressed(res)=="help":
				self.show_help_main()
			if bb.buttonPressed(res)=="quit":
				self.screen.finish()
				sys.exit(1)

	# Text Message PopUp
	def messagebox(self, message, title="Error"):
		error_message = TextboxReflowed(55,message)
		bb = ButtonBar(self.screen,((("Back"),"back","F12"),))
		tl = GridForm(self.screen,title,1,3)
		tl.add(error_message,0,0)
		tl.add(bb,0,1,growx=1)
		res = tl.run()
		while 1:
			if bb.buttonPressed(res)=="back":
				self.screen.popWindow()
				break

	# Two Factor Menu
	def show_two_factor(self):
		while 1:
			self.twofactor = EntryWindow(self.screen, "Two Factor Authentication", "Please enter the following information to enable Two Factor Authentication:", ["Username","Entity Account"], 0, 40, 20, ['Ok','Cancel'])
			if self.twofactor[0] == 'ok' and (self.verify.check_username(self.twofactor[1][0]) == False or self.verify.check_username(self.twofactor[1][1]) == False):
				self.show_two_factor_error()
			elif self.twofactor[0] == 'ok' and self.verify.check_username(self.twofactor[1][0]) == True and self.verify.check_username(self.twofactor[1][1]) == True:
				self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] = 'Y'
				self.data['DEFAULT_COE_TWO_FACTOR_USERNAME'] = self.twofactor[1][0]
				self.data['DEFAULT_COE_TWO_FACTOR_ENTITY'] = self.twofactor[1][1]
				break
			elif self.twofactor[0] == 'Cancel':
				self.data['DEFAULT_COE_TWO_FACTOR_CONFIG'] = 'N'
				self.refresh_menus()
				break
			
	# Two Factor Veification
	def show_two_factor_error(self):
		self.error_text = "The following input has problem(s):\n"
		if self.verify.check_username(self.twofactor[1][0]) == False:
			self.error_text += "  - Userame\n"
		if self.verify.check_username(self.twofactor[1][2]) == False:
			self.error_text += "  - Entity Account\n"
		self.messagebox(self.error_text)


	# Updates Exceptions by checking current State
	def check_state(self, var):
		for i in eval("config.%s.keys()"%(var)):
			if eval("len(config.%s[%d])"%(var,i)) > 3:
				if eval("config.%s[%d][3]"%(var,i)) != None:
					for j in eval("config.%s[%d][3]"%(var,i)):
						for k in eval("config.Exception[%d][2]"%(j)):
							if eval("self.%s%d.value()"%(var,i)) == 1:
								if eval("config.Exception[%d][1]"%(j)) == "default_on":
									exec("self.%s%d.setValue('*')"%(var,k))
									exec("self.%s%d.setFlags(FLAG_DISABLED,FLAGS_SET)"%(var,k))
									exec("self.%s%d_disabled = 1"%(var,k))
							elif eval("self.%s%d.value()"%(var,i)) == 0:
								if eval("config.Exception[%d][1]"%(j)) == "default_off":
									exec("self.%s%d.setValue(' ')"%(var,k))
									exec("self.%s%d.setFlags(FLAG_DISABLED,FLAGS_SET)"%(var,k))
									exec("self.%s%d_disabled = 1"%(var,k))
								elif eval("config.Exception[%d][1]"%(j)) == "off":
									exec("self.%s%d.setValue(' ')"%(var,k))
									exec("self.%s%d.setFlags(FLAG_DISABLED,FLAGS_SET)"%(var,k))
									exec("self.%s%d_disabled = 1"%(var,k))


	# Runs Through Exceptions Defined in Configuration File	
	def update_exceptions(self, option):
		# Splits Data Since I can't pass multiple values from a callback function.
		data = option.split(':',2)
		on = "*"
		off = " "
		if eval("len(config.%s[%s])"%(data[0],data[1])) > 3:
			if eval("config.%s[%s][3]"%(data[0],data[1])) != None:
				for i in eval("config.%s[%s][3]"%(data[0],data[1])):
					for j in eval("config.Exception[%s][2]"%(i)):
						if eval("self.%s%s.value()"%(data[0],data[1])) == 1:
							if eval("config.Exception[%d][1]"%(i)) == "checked" and eval(eval("'self.'+config.Exception[%d][0]+'%d_disabled'"%(i,j))) != 1:
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(on)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "unchecked" and eval(eval("'self.'+config.Exception[%d][0]+'%d_disabled'"%(i,j))) != 1:
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(off)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "disabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_SET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 1'"%(i,j)))

							elif eval("config.Exception[%d][1]"%(i)) == "enabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
								if eval(eval("'self.data_original[config.'+config.Exception[%d][0]+'[%d][1]]'"%(i,j))) == 'Y':
									exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(on)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(on)'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(on)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_SET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 1'"%(i,j)))
						elif eval("self.%s%s.value()"%(data[0],data[1])) == False:
							if eval("config.Exception[%d][1]"%(i)) == "disabled":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(off)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_SET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 1'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(off)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_off":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(off)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_SET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 1'"%(i,j)))
							elif eval("config.Exception[%d][1]"%(i)) == "default_on":
								exec(eval("'self.'+config.Exception[%d][0]+'%d.setFlags(FLAG_DISABLED,FLAGS_RESET)'"%(i,j)))
								exec(eval("'self.'+config.Exception[%d][0]+'%d_disabled = 0'"%(i,j)))
								if eval(eval("'self.data_original[config.'+config.Exception[%d][0]+'[%d][1]]'"%(i,j))) == 'Y':
									exec(eval("'self.'+config.Exception[%d][0]+'%d.setValue(on)'"%(i,j)))

                                                # Deals with childern
                                                if eval(eval("'len(config.'+config.Exception[%d][0]+'[%d])'"%(i,j))) > 3:
                                                        if eval(eval("'config.'+config.Exception[%d][0]+'[%d][3]'"%(i,j))) != None:
                                                                for k in eval(eval("'config.'+config.Exception[%d][0]+'[%d][3]'"%(i,j))):
                                                                        exec("self.update_exceptions('%s:%d')"%(data[0],j))

		self.refresh_options(data[0])

	# Refreshes Screen Options to fix a bug with Snack
	def refresh_options(self, var):	
		for item in eval("config.%s.keys()"%(var)):
			if eval("self.%s%d.value()"%(var, item)) == 1:
				exec("self.%s%d.setValue('*')"%(var,item))
			else:
				exec("self.%s%d.setValue(' ')"%(var,item))
			self.update_config()

        # Refresh Menu Options
        def refresh_menus(self):
		# Initialize Option Menu
                for item in reversed(config.Option.keys()):
			if self.data[config.Option[item][1]] == 'Y':
                                exec("self.Option%d.setValue('*')"%(item))
                        elif self.data[config.Option[item][1]] == 'N':
                                exec("self.Option%d.setValue(' ')"%(item))

        # Updates Configuration for Menu Items
        def update_config(self):
		# Update Advanced Option Menu
                for item in config.Option.keys():
			if eval("self.Option%d.value()"%(item)) == 1:
				if eval("len(config.Option[%s])"%(item)) > 4 and eval("self.data[config.Option[%s][1]]"%(item)) == 'N':
					exec(eval("config.Option[%s][4]"%(item)))
				exec("self.data.update({config.Option[%s][1]:'Y'})"%(item))
                        else:
				exec("self.data.update({config.Option[%s][1]:'N'})"%(item))
 
        # Appply COE to Kickstart File
        def apply_coe(self):
		# Make sure a LAN was selected
		if self.lan_select.current() == "":
			self.messagebox("Please select a LAN!","ERROR")
			return False

		self.data["DEFAULT_COE_LAN"] = self.lan_select.current()
		self.cfg.set_config("/etc/sandia/.ksswitches", self.data)
		self.screen.finish()
		sys.exit(0)

        # Shows Help for Main Install
        def show_help_main(self):
                help_text = ("Linux COE Configuration.\n\nConfigures Sandia's Approved Hardening Scripts and Configurations on a Red Hat Enterprise Linux Install. For more information and docuementation please vist the COE Homepage:\n\nhttp://redhatcoe.sandia.gov")
                self.messagebox(help_text, "Linux COE Help")

# Executes Window Display
if __name__ == "__main__":
        coe = MainMenu()
