#!/usr/bin/python

PROGNAME='media-prompt-graphical'

from snack import *
import gettext, sys, gtk
gettext.bindtextdomain(PROGNAME, "/usr/share/locale")
gettext.textdomain(PROGNAME)

try:
    gettext.install(PROGNAME, "/usr/share/locale", 1)
    import __builtin__
    __builtin__.__dict__['_'] = gettext.gettext
except IOError:
    import __builtin__
    __builtin__.__dict__['_'] = unicode

# Display Main Menu
class MainMenu:
        def __init__(self):
                # Create Main Window
                self.window = gtk.Window()
                self.window.set_title("Sandia National Labs - Red Hat Enterprise Linux COE")
                self.window.set_position(gtk.WIN_POS_CENTER)
		self.window.set_icon_from_file(self.get_base_path()+'/sandia16.png')
		self.window.connect("delete_event",gtk.main_quit)
		self.display = gtk.gdk.display_get_default()
		self.screen = self.display.get_default_screen()
		self.hres = self.screen.get_width()
		self.vres = self.screen.get_height()
		self.window.resize(530,100)

                # Create Main Vertical Box to Populate
                self.vbox = gtk.VBox()

	        # Creates Logo and COE Header
                self.header = gtk.HBox()

                self.logo = gtk.Image()
                self.logo.set_from_file(self.get_base_path()+'/snl_logo.png')
                self.header.add(self.logo)

		self.header_text = gtk.VBox()
                self.label = gtk.Label("<span font_family='Airal' weight='bold' foreground='red' size='large'>  Red Hat Enterprise Linux COE </span>\n<b>Media Prompt</b>")
                self.label.set_use_markup(True)
		self.label.set_justify(gtk.JUSTIFY_CENTER)
                self.header.add(self.label)
 		self.vbox.add(self.header)

        	prompt_msg = ("\nYou have reached this screen due to one of the following reasons:\n\n" +
                " - You are installing the Linux COE from Standalone Media \n" +
                " - The Standalone Media is OUT OF DATE\n" +
		" - This workstation is not configured for the SRN LAN\n\n" +
		"Note:   The Standalone Media is required for the SON/SCN\n\n" +
		"The network or COE server could not be reached (Perhaps the network\ncable is nunplugged, the network is not configured properly, the network\nis down or firewalled).\n\n"+
		"You may either insert the StandAlone CD, check network cables or\nconfiguration and press OK to try again, or Quit now (you can always run the\n'linuxcoe' script again).\n")
		
		self.label = gtk.Label(prompt_msg)
		self.vbox.pack_start(self.label, False, False, 0)

		# Button Bar at the Bottom of the Window
                self.button_bar = gtk.HBox()

                # Apply COE
                self.button1 = gtk.Button(None, gtk.STOCK_OK)
                self.button1.connect("clicked", self.apply)
                self.button_bar.pack_end(self.button1, False, True, 0)

                # Quit - Debug - Progam Quits in Apply COE
                self.button2 = gtk.Button(None, gtk.STOCK_QUIT)
                self.button2.connect("clicked", self.quit)
                self.button_bar.pack_end(self.button2, False, True, 0)

                self.vbox.add(self.button_bar)

                self.window.add(self.vbox)
                self.window.show_all()

	# Key Press Event
	def event_key(self, args, event):
		if event.keyval == gtk.keysyms.F12:	
			self.apply_coe(args)
		elif event.keyval == gtk.keysyms.F1:
			self.show_help_main(args)

	# Get Base Path
	def get_base_path(self):
		index = sys.argv[0].rfind('/')
 		path = sys.argv[0][0:index]
		return path

	# Quit
	def quit(self,args):
		gtk.main_quit()
		sys.exit(1)

	# Continue
	def apply(self,args):
		gtk.main_quit()
		sys.exit(0)

# Executes Window Display
if __name__ == "__main__":
        coe = MainMenu()
        gtk.main()
