# config.py - Configurations for the linuxcoe-text.py and linuxcoe-graphical.py

# COE OPTIONS
# Displays COE Congfiguration Options
# 1. Display Text - String - Text Displayed next to check box
# 2. Environment - String - Contains the Environment Variable to write to.
# 3. Parent Index - Integer - Parent for child options in the array (Integer or None [Default]) (Optional)
#                   Parent Exception List over rules any other outside exceptions
# 4. Exceptions Index - Array of Integers - Contains the index of the Exceptions (Optional)
# 5. Functions - String - Runs a function when Option value is toggled to True. (Optional)

Option = {}
Option[0] = ("Install a Common Desktop Environment/Applications","DEFAULT_COE_COMMON_DESKTOP",None)
Option[1] = ("Apply the Latest Security Configurations","DEFAULT_COE_APPLY_SECURITY",None)
#Option[2] = ("Enable Syslog Advanced Logging","DEFAULT_COE_SYSLOG",None)
Option[2] = ("Enable a Default Stateful Firewall (Replace IPTables Configuartion)","DEFAULT_COE_FIREWALL",None)
Option[3] = ("Apply the Latest Tested Patches","DEFAULT_COE_UPDATE_PACKAGES",None)
Option[4] = ("Upgrade to the Latest Tested Kernel Version","DEFAULT_COE_UPGRADE_KERNEL",None)
Option[5] = ("Register Machine on SNL's RHN Satellite Server (Recommended)","DEFAULT_COE_UP2DATE_SERVER",None)
Option[6] = ("Enable and Configure SNL Kerberos Authenication","DEFAULT_COE_KERBEROS_AUTHENTICATION",None,[0])
Option[7] = ("Configure and Apply Two Factor Authentication (sudousers)","DEFAULT_COE_TWO_FACTOR_CONFIG",None, None,"self.show_two_factor()")

# EXCEPTIONS
# Creates a way to control items for menus
# 1. Array Name - String -Links to array to control
# 2. Status - String - Operation that should be done on items
#             when checked, oppisite is done when unchecked.
#       a.) "checked" - checks all toggle boxes in array
#       b.) "unchecked" - unchecks all toggle boxes in array
#       c.) "disabled" - disables all checkboxes in array
#       d.) "enabled" - enables all checkboxes in array
#       e.) "off" - unchecked + disabled
#       f.) "on" - checked + enabled
#       g.) "default_on" - turns "on" (disabled) all items when checked, returns all items to default when unchecked.
#       h.) "default_off" - returns all items to default values when checked, turns "off" all items when unchecked.
# 3. Index List - Array of Integers - Index for Array Name to preform the status on.

Exception = {}
Exception[0] = ("Option","off",[7])
