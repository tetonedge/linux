#!/usr/bin/python

PROGNAME="linuxcoe-boottime"

from snack import *
import gettext
import os
import sys
import re
import time
gettext.bindtextdomain(PROGNAME, "/usr/share/locale")
gettext.textdomain(PROGNAME)
try:
    gettext.install(PROGNAME, "/usr/share/locale", 1)
    import __builtin__
    __builtin__.__dict__['_'] = gettext.gettext
except IOError:
    import __builtin__
    __builtin__.__dict__['_'] = unicode


class mainScreen:

   def __init__(self,screen):

	self.screen = screen
	self.inittime = time.time()

	self.description = (_("It is strongly recommended that you run the Linuxcoe script to "
			    "configure your machine to comply with SNL's latest security mandates, "+
			    "additional security configurations, and Common Operating Environment.  "+
			    "\n\n"+
                            "If you choose not to run the Linuxcoe script at this time, or if it does not run to "+
			    "completion, the Linuxcoe script can be ran at a later time."))
	self.timeout_length = 11

   def updateTimeout(self,arg):

	if arg == 1:
	   return

	new_time = int(self.inittime-time.time()+self.timeout_length)

	if new_time == 1:
	    self.timeout.setText("Linuxcoe will run in "+str(new_time)+ " second.")
	elif new_time == 0:
	    self.timeout.setText("Linuxcoe will now run.")
	else:
	    self.timeout.setText("Linuxcoe will run in "+str(new_time)+ " seconds.")

   def runIt(self):

	table = GridForm(self.screen,_("Linuxcoe"),1,3)
	g1 = Grid(1,1)
	g2 = Grid(1,1)

	self.text = TextboxReflowed(68,self.description)
	self.timeout = Label("Linuxcoe will run in "+str(self.timeout_length)+" seconds")
	self.buttonbar = ButtonBar(self.screen,((_("Ok"),"ok","F12"),(_("Menu"),"switches","m"),(_("Use Standalone CD"),"standalone","s"),(_("Don't Run"),"never","d"),(_("Pause"),"pause","p")))

	g1.setField(self.text,0,0,(0,0,0,1))
	g2.setField(self.timeout,0,0,(0,0,0,1))

	table.add(g1,0,0)
	table.add(g2,0,1)
	table.add(self.buttonbar,0,2)
	table.setTimer(1)

	something_pressed = 0
	switches_pressed = 0
        standalone_pressed = 0

	temp = 1
        args = ""
        while temp <  len(sys.argv):
           args = args + sys.argv[temp] + " "
           temp = temp + 1

	while self.inittime-time.time()+self.timeout_length>0 or something_pressed:

	   self.updateTimeout(something_pressed)
	   response = table.run()   
	
	   if self.buttonbar.buttonPressed(response) == "standalone":
		standalone_pressed = 1
		break
	   elif self.buttonbar.buttonPressed(response) == "switches":
		switches_pressed = 1
		break
	   elif self.buttonbar.buttonPressed(response) == "ok":
		break
	   elif self.buttonbar.buttonPressed(response) == "never":
		screen.finish()
		os.system("/sbin/chkconfig linuxcoe off")
                os.system("reboot")
		return
	   elif self.buttonbar.buttonPressed(response) == "pause":
		something_pressed = 1
		self.timeout.setText("")

	if switches_pressed == 0:
	   args = args + " noswitches"

      	if standalone_pressed == 1:
	   args = args + " standalone"
	   

	screen.finish()
	status = os.system("/opt/sandia/linuxcoe/linuxcoe "+args)
	return status

try:
   screen = SnackScreen()
   ms = mainScreen(screen)
   status = ms.runIt()
except:
   screen.finish()
   print "Error"
