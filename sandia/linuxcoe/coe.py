#!/usr/bin/python

import os, re

# Class containing verification items
class Verification:
	# Function: check_fullname
	# Purpose:  Check for vaild first and last name
	# Inputs:   fullname (string to verify)
	# Outputs:  True/False Value	
	def check_fullname(self, fullname):
		pattern = re.compile(r"^[ a-zA-Z']+$", re.VERBOSE)
		if re.match(pattern, fullname):
			return True
		else:
			return False

	# Function: check_username
	# Purpose:  Check for vaild Unix username
	# Inputs:   ip (string to verify)
	# Outputs:  True/False Value	
	def check_username(self, username):
		pattern = re.compile(r"^\w{5,255}$", re.VERBOSE)
		if re.match(pattern, username):
			return True
		else:
			return False

	# Function: check_ip
	# Purpose:  Check for vaild Unix UID
	# Inputs:   uid (integer to verify)
	# Outputs:  True/False Value	
	def check_uid(self, uid):
		pattern = re.compile(r"^\d{1,10}$", re.VERBOSE)
		if re.match(pattern, uid):
			return True
		else:
			return False

	# Function: check_ip
	# Purpose:  Check for vaild IP address
	# Inputs:   ip (string to verify)
	# Outputs:  True/False Value		
	def check_ip(self, ip):
		pattern = re.compile(r"\b(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-3])\b", re.VERBOSE)
		if re.match(pattern, ip) and ip != "0.0.0.0":
			return True
		else:
			return False

        # Function: check_hostname
        # Purpose:  Check for vaild system hostanme
        # Inputs:   hostname (string to verify)
        # Outputs:  True/False Value
        def check_hostname(self, hostname):
                pattern = re.compile(r"^[a-zA-Z0-9\-\.]{1,100}$", re.VERBOSE)
                if re.match(pattern, hostname):
                        return True
                else:
                        return False

# Class containing configuration items
class Configuration:
	# Function: get_env_var
	# Purpose:  Return a single value from the Operating System
	# Inputs:   key (string, OS Environment variable - eg PATH)
	# Outputs:  value (string)
	def get_env_var(self, key):
		return os.environ[key]
	
	# Function: get_env
	# Purpose:  Returns environment key values from the Operating System
	# Inputs:   None
	# Outputs:  var (dictionary, contains entire environment for OS)
	def get_env(self):
		var = {}
		for key in os.environ.keys():
			var.update({key:os.environ[key]})	
		return var

	# Function: get_env_group
	# Purpose:  Returns environment key values for search from the Operating System
	# Inputs:   search (string, filter value)
	# Outputs:  var (dictionary, contains environment variables filtered by search)
	def get_env_group(self, search):
		var = {}
		for key in os.environ.keys():
			if key.find(search) >= 0:
				var.update({key : os.environ[key]})	
		return var

	# Function: get_custom_env_group
        # Purpose:  Returns environment key values for search from the Operating System
        # Inputs:   list (array, list of varibles to get)
        # Outputs:  var (dictionary, contains environment variables filtered by search)
        def get_custom_env_group(self, list):
                var = {}
                for key in os.environ.keys():
                        if key in list:
                                var.update({key : os.environ[key]})
                return var

	# Function: get_config
	# Purpose:  Reads in environment varibles from Operating System or file
	# Inputs:   file (string, pathname to file,required)
	# Outputs:  var (dictionary)         
	def get_config(self, file):
		var = {}
		if os.path.exists(file):
			f = open(file, "r")
			for line in f.readlines():
				field = line.split("=")
				var.update({field[0].strip():field[1].strip()})
			f.close()
		return var
	
	# Function: set_config
	# Purpose:  Writes varibles to file, will destory exisiting file
	# Inputs:   file (string, required),var (dictionary, required),
	#	    append (int, optional, 0[default] create, 1 append	
	# Outputs:  success/failiure
	def set_config(self, file, var, append=0):
                pattern = re.compile(r"^[a-zA-Z0-9\,\-\._]+$", re.VERBOSE)
		if append == 0:
			f = open(file, "w")
		elif apppend == 1:
			f = open(file, "a")
		for key, val in var.items():
			if val != "":
				if re.match(pattern, str(val)):
					print >> f,"%s=%s"%(key,val)
				else:
					print >> f,"%s=\"%s\""%(key,val)
		f.close()

	# Function: set_custom_config
	# Purpose:  Writes a list of varibles to file, will destory exisiting file
	# Inputs:   file (string, required),var (dictionary, required), list (array, list of keys, required)
	#	    append (int, optional, 0[default] create, 1 append	
	# Outputs:  success/failiure
	def set_custom_config(self, file, var, list, append=0):
		pattern = re.compile(r"^[a-zA-Z0-9\-\._]+$", re.VERBOSE)
		if append == 0:
			f = open(file, "w")
		elif apppend == 1:
			f = open(file, "a")
		for key, val in var.items():
			if key in list:
				if val != "":
					if re.match(pattern, str(val)):
						print >> f,"%s=%s"%(key,val)
					else:
						print >> f,"%s=\"%s\""%(key,val)
		f.close()
