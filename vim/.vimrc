autocmd! bufwritepost .vimrc source %
execute pathogen#infect()
execute pathogen#helptags()
syntax enable
let mapleader = ","
let maplocalleader = "\\"
" Show whitespace
" MUST be inserted BEFORE the colorscheme command
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/

"" coloring
if has('gui_running')
    set background=light
else
    set background=dark
endif
colorscheme solarized

"" line numbers
set number

" easier formatting of paragraphs
vmap Q gq
nmap Q gqap

nnoremap <F5> :GundoToggle<CR>
nnoremap <C-d> :YcmCompleter GoToDefinition<CR>

" Useful settings
set history=700
set undolevels=700

"" width of document
set tw=80 
set colorcolumn=80
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set nobackup
set nowritebackup
set noswapfile
set laststatus=2
set hlsearch

" Better copy & paste
" When you want to paste large blocks of code into vim, press F2 before you
" paste. At the bottom you should see ``-- INSERT (paste) --``.
set pastetoggle=<F2>
set clipboard=unnamed

" Bind nohl
" Removes highlight of your last search
" ``<C>`` stands for ``CTRL`` and therefore ``<C-n>`` stands for ``CTRL+n``
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>
" Quicksave command
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>
" Quick quit command
noremap <Leader>e :quit<CR> " Quit current window
noremap <Leader>E :qa!<CR> " Quit all windows
" bind Ctrl+<movement> keys to move around the windows, instead of using Ctrl+w + <movement>
" Every unnecessary keystroke that can be saved is good for your health :)
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h
" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

set guifont=SauceCodePowerline\ 10


" map sort function to a key
vnoremap <Leader>s :sort<CR>

" easier moving of code blocks
" Try to go into visual mode (v), thenselect several lines of code here and
" then press ``>`` several times.
vnoremap < <gv " better indentation
vnoremap > >gv " better indentation

filetype plugin indent on

python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>
map <F3> :NERDTreeToggle<CR>

nnoremap <buffer> <F10> :exec '!python2' shellescape(@%, 1)<cr>

if !exists("autocommands_loaded")
let autocommands_loaded = 1
autocmd BufRead,BufNewFile,FileReadPost *.py source ~/.vim/python.vim
endif

let g:SimpylFold_docstring_preview = 1
let g:latex_to_unicode_tab = 0
let g:gitgutter_enabled = 1

" Latex
" forward search with mupdf
function! MuPDFForward()
    lcd %:p:h
    let pageforward = 0
    let searchline = line(".") + 1
    let searchcol = col(".") + 1
    let pageforward = system("synctex view -i " . searchline . ":" . searchcol . ":\"" . expand('%:p:h') . '/./' . expand('%:t') . "\" -o \"" . g:latex#data[b:latex.id].out() . "\" | grep -m1 'Page:' | sed 's/Page://' | tr -d '\n'")
    if pageforward > 0
    "echo "Jumping to page " . pageforward
    silent call system("xdotool search --name " . g:latex#data[b:latex.id].name . ".pdf key " . pageforward . "+g")
    endif
endfunction

function! MuPDFReverse()
    lcd %:p:h
    let activepage = 0
    let activepage = system("xdotool search --name " . g:latex#data[b:latex.id].name . ".pdf getwindowname | sed 's:.* - \\([0-9]*\\)/.*:\\1:' | tr -d '\n'")
    if activepage > 0
        let inputfile = system("synctex edit -o \"" . activepage . ":288:108:" . g:latex#data[b:latex.id].out() . "\" | grep 'Input:' | sed 's/Input://2' | head -n1 | tr -d '\n' 2>/dev/null")
        if inputfile =~ expand("%")
            let gotoline = 0
                let gotoline = system("synctex edit -o \"" . activepage . ":288:108:" . g:latex#data[b:latex.id].out() . "\" | grep -m1 'Line:' | sed 's/Line://' | head -n1 | tr -d '\n'")
            if gotoline > 0
                exec ":" . gotoline
                echo "Went to line " . gotoline
            endif
        else
            echo "Synctex reported an external location - " . inputfile
        endif
    else
        echo "Cannot read MuPDF page number."
    endif
endfunction 

nnoremap <silent> <buffer> <LocalLeader>f :call MuPDFForward()<CR>
nnoremap <silent> <buffer> <LocalLeader>r :call MuPDFReverse()<CR>
