# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/Candidate.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/Candidate.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/CandidateRepository.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/CandidateRepository.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/CustomAssert.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/CustomAssert.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierCompleter.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/IdentifierCompleter.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierDatabase.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/IdentifierDatabase.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/IdentifierUtils.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/IdentifierUtils.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/LetterNode.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/LetterNode.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/LetterNodeListMap.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/LetterNodeListMap.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/PythonSupport.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/PythonSupport.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/Result.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/Result.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/Utils.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/Utils.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/versioning.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/versioning.cpp.o"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/ycm_client_support.cpp" "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/ycm/CMakeFiles/ycm_client_support.dir/ycm_client_support.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "BOOST_THREAD_DONT_USE_CHRONO"
  "USE_CLANG_COMPLETER"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jdtuck/Downloads/YouCompleteMe/ycm_build/BoostParts/CMakeFiles/BoostParts.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/BoostParts"
  "/usr/include/python2.6"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/llvm/include"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm"
  "/home/jdtuck/Downloads/YouCompleteMe/third_party/ycmd/cpp/ycm/ClangCompleter"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
